package laboratorio;

public class Circulo {
	
	private Punto centro;
	private double radio;
	
	/**
        * Constructor sin parametros     
        */
	public Circulo(){
		centro = new Punto(0,0);
		radio = 0;
	}
	
        /**
        * Constructor con parameros     
        * @param p  tipo Punto
        * @param rad tipo double
        */
	public Circulo(Punto p, double rad){
		//centro.setX(p.getX());
		//centro.setY(p.getY());
		centro = p;
		radio = rad;		
	}
	
        
        /**
        * Constructor con parameros     
        * @param px  tipo double
        * @param py  tipo double
        * @param rad  tipo double
        */
	public Circulo(double px, double py, double rad){
		//centro.setX(px);
		//centro.setY(py);
		centro = new Punto(px, py);
		radio = rad;
	}
	
	
	//getters y setters
	public Punto getCentro() {
		return centro;
	}
	public void setCentro(Punto centro) {
		this.centro = centro;
	}
	public double getRadio() {
		return radio;
	}
	public void setRadio(double radio) {
		this.radio = radio;
	}
	
	
	//Metodos
	public double calcularDistanciaDesde(Punto desde){
		double valorX;
		double valorY;
		
		valorX = Math.pow((centro.getX() - desde.getX()), 2);
		valorY = Math.pow((centro.getY() - desde.getX()), 2);
		
		return Math.sqrt(valorX + valorY);
	}
	
	public double calcularArea(){
		return Math.PI * (Math.pow(this.getRadio(),2));
	}
	
	public double calcularPerimetro(){
		return Math.PI * (2 * this.getRadio());
	}
	

}
