package laboratorio;
/**
 * @author seba traverso
 * @version 1.1
 */
public class Punto {
        /**
        * Variable privada: Nombre del autor
        */
	private double x;
	private double y;
	
	/**
        * Constructor sin parameros     
        */
	public Punto(){
		this.setX(0);
		this.setY(0);
	}
	
        /**
        * Constructor con parameros     
        * @param valorX tipo double
        * @param valorY tipo double
        */
	public Punto(double valorX, double valorY){
		this.setX(valorX);
		this.setY(valorY);
	}
	
	
	/**
        * Setters y Getters
        */
        /**
        * @return double valorx
        */
	public double getX() {
		return x;
	}
        /**
        * @param x double
        */
	public void setX(double x) {
		this.x = x;
	}
	/**
        * @return double valory
        */
        public double getY() {
		return y;
	}
        /**
        * @param y double
        */
	public void setY(double y) {
		this.y = y;
	}
	
	
	/**
        * Metodo calcularDistanciaDesde
        * @param desde tipo Punto
        * @return valor double
        */
	public double calcularDistanciaDesde(Punto desde){
		double valorX;
		double valorY;
		
		valorX = Math.pow((this.getX() - desde.getX()), 2);
		valorY = Math.pow((this.getY() - desde.getX()), 2);
		
		return Math.sqrt(valorX + valorY);
	}
	
	

}
