package unidad2;

public class Punto {
	private double x;
	private double y;
	
	//Constructores
	public Punto(){
		this.setX(0);
		this.setY(0);
	}
	
	public Punto(double valorX, double valorY){
		this.setX(valorX);
		this.setY(valorY);
	}
	
	
	//Setters y Getters
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	
	
	//Metodos
	public double calcularDistanciaDesde(Punto desde){
		double valorX;
		double valorY;
		
		valorX = Math.pow((this.getX() - desde.getX()), 2);
		valorY = Math.pow((this.getY() - desde.getX()), 2);
		
		return Math.sqrt(valorX + valorY);
	}
	
	

}
