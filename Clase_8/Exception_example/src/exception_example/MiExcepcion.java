/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exception_example;

/**
 *
 * @author anonimo
 */
public class MiExcepcion extends Exception{
    String mensaje;
    
    public MiExcepcion(String mensaje) {
        this.mensaje = mensaje;
    }
    
    public MiExcepcion() {
        super("mi Excepcion");
    }
    
    public String getMensaje() {
    return mensaje;
    }

}
