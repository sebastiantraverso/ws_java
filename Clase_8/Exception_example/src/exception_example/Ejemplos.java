/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exception_example;

import java.util.InputMismatchException;
import java.util.Scanner;



/**
 *
 * @author anonimo
 */
public  class Ejemplos {
    
    int[] array = {0, 1, 2, 3, 4, 5};
    
    public Ejemplos() {
        
    }
    
    public double ejemplo_7()throws ValorNoValido {
          Scanner sc = new Scanner(System.in);
          System.out.print("Introduce número > 0 ");
          double n = sc.nextDouble();
          if (n <= 0) {
              throw new ValorNoValido("El número debe ser positivo");
          }
          return n;
    }
    
    public void ejemplo_6(){
    Scanner sc = new Scanner(System.in);
    int [] array = {4,2,6,7};
    int n;
    boolean repetir = false;
    do{
         try{
                repetir = false;
                System.out.print("Introduce un número entero > 0 y < " + array.length + " ");
                n = sc.nextInt();
                System.out.println("Valor en la posición " + n + ": " + array[n]);
         }catch(InputMismatchException e){
                   sc.nextLine();
                   n = 0;
                   System.out.println("Debe introducir un número entero ");
                   repetir = true;
         }catch(IndexOutOfBoundsException e){
                  System.out.println("Debe introducir un número entero > 0 y < " + array.length + " ");
                  repetir = true;
         }catch(Exception e){ //resto de excepciones de tipo Exception y derivadas
                   System.out.println("Error inesperado " + e.toString());
                   repetir = true;
         }
     }while(repetir);
    }
    
    public void ejemplo_5(){
     Scanner sc = new Scanner(System.in);
      int n;
      do{
           try{
                 System.out.print("Introduce un número entero > 0: ");
                 n = sc.nextInt();
                 System.out.println("Número introducido: " + n);
          }catch(InputMismatchException e){
                       sc.nextLine();
                       n = 0;
                       System.out.println("Debe introducir un número entero " + e.toString());
          }
      }while(n<=0);
    }
    
    public int ejemplo_3(int indice){
    return this.array[indice];
    }
    
    public String ejemplo_2(){
        String s = "ejemplo 2 sobre Excepciones";
        return s;
    }
    
    public static void ejemplo_1(){
       try {
            throw new Exception("Esto es una Excepcion");
       } catch(Exception e) {
            System.out.println("Se produjo un excepcion: " + e.getMessage());
       } finally {
            System.out.println("Esto se ejecuta sin importar si se presentan errores");
       }
   }


    
    
    
}
