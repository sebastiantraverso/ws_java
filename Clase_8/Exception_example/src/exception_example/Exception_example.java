/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exception_example;

/**
 *
 * @author anonimo
 */
public class Exception_example {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       // Ejemplo 1 de excepciones 
       //Ejemplos.ejemplo_1(); 
/*       
       // Ejemplo 2 de excepciones
       Ejemplos eje = null;
       try {
            eje.ejemplo_2();
       } catch (Exception e){
            System.out.println("Se produjo una expción. " + e.getMessage());
       }
*/
 /* 
        //Ejemplo 3 de excepciones
        Ejemplos eje = new Ejemplos();
        try {
            eje.ejemplo_3(11111);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Excepción: indice de array fuera de limites " + e.getMessage());
        }
*/  
/*      
        //Ejemplo_4        
        try {
            throw new MiExcepcion("mi propia Excepción");
        } catch(MiExcepcion e) {
            System.out.println("excepción: " + e.getMensaje());
        }        
*/
 /*
        //Ejemplo_5
        Ejemplos eje = new Ejemplos();
        eje.ejemplo_5();
*/
/*        
        //Ejemplo_6
        Ejemplos eje = new Ejemplos();
        eje.ejemplo_6();
*/

        //Ejemplo_7
        Ejemplos eje = new Ejemplos();
        try {
            double x = eje.ejemplo_7();
            System.out.println("Raiz cuadrada de " + x + " = " + Math.sqrt(x));
        }catch (ValorNoValido e) {
                System.out.println(e.getMessage());
        }
   



    }
}
