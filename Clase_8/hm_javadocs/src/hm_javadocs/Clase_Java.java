package hm_javadocs;
/**
 * @author Maximiliano Correa   
 * @see <a href="www.linkedin.com/in/cmcorrea">www.linkedin.com/in/cmcorrea</a>
 * @version 1.2 26 de Agosto 2017
 */
public class Clase_Java {
    /**
    * Variable privada: Nombre del autor
    */
    private final String Autor;
    
    /**
    * Variable publica: Una frase para reflexionar
    */
    public String Frase_del_dia;
    /**
     * Constructor 1 de clase     
     */
    public Clase_Java() {
        Autor = "Charles Sanders Peirce";
        Frase_del_dia = "Toda la evolución que conocemos procede de lo vago a lo definido \n" + Autor;
    }
    /**
     * Constructor 2 de clase     
     * @param msg corresponde al mensaje del dia
     * @param Autor corresponde al autor de la frase
     */
    public Clase_Java(String msg, String Autor) {
        this.Autor = Autor;
        this.Frase_del_dia = msg + " " + Autor;
    }
    /**
    * Retorna la Frase del dia   
    * @return String Frase del dia
    */
    public String getFrase_del_dia() {
        return Frase_del_dia;
    }
    /**
    *Setea una nueva frase del dia :) 
    *   
    * @param Frase_del_dia  La nueva frase del dia.
    */
    public void setFrase_del_dia(String Frase_del_dia) {
        this.Frase_del_dia = Frase_del_dia;
    }
   
    



}
