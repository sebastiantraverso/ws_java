package unidad2;

public class Triangulo {
	private Punto p1;
	private Punto p2;
	private Punto p3;
	
	
	//Constructores
	public Triangulo(){
		p1 = new Punto(0,0);
		p2 = new Punto(5,0);
		p3 = new Punto(0,5);
	}
	
	public Triangulo(Punto paramP1, Punto paramP2, Punto paramP3){
		p1 = new Punto(paramP1.getX(), paramP1.getY());
		p2 = new Punto(paramP2.getX(), paramP2.getY());
		p3 = new Punto(paramP3.getX(), paramP3.getY());
	}
	
	public Triangulo(double x1, double y1, double x2, double y2, double x3, double y3){
		p1 = new Punto(x1, y1);
		p2 = new Punto(x2, y2);
		p3 = new Punto(x3, y3);
	}
	
	//Setters y Getters
	public Punto getP1() {
		return p1;
	}
	public void setP1(Punto p1) {
		this.p1 = p1;
	}
	public Punto getP2() {
		return p2;
	}
	public void setP2(Punto p2) {
		this.p2 = p2;
	}
	public Punto getP3() {
		return p3;
	}
	public void setP3(Punto p3) {
		this.p3 = p3;
	}
	
	
	//Metodos
	public double calcularDistanciaDesde(Punto desde){
		double valorX;
		double valorY;
		
		valorX = Math.pow((p1.getX() - desde.getX()), 2);
		valorY = Math.pow((p1.getY() - desde.getX()), 2);
		
		return Math.sqrt(valorX + valorY);
	}
	
	public double calcularArea(){
		double izqDer = 0;
		double derIzq = 0;
		double areaTotal;
		
		izqDer = (this.getP1().getX() * this.getP2().getY()) + 
				 (this.getP2().getX() * this.getP3().getY()) +
				 (this.getP3().getX() * this.getP1().getX());
		
		derIzq = (this.getP1().getY() * this.getP2().getX()) +
				 (this.getP2().getY() * this.getP3().getX()) +
				 (this.getP3().getY() * this.getP1().getX());
		
		areaTotal = izqDer - derIzq;
		
		if (areaTotal < 0)
			areaTotal = areaTotal * (-1);
		
		return areaTotal;
	}
	
	public double calcularPerimetro(){
		double a = 0;
		
		return a;
	}

}
