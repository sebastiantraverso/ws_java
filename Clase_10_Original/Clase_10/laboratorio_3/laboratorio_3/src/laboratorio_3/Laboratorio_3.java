/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio_3;

import trigonomietria.basica.*;


/**
 *
 * @author anonimo
 */
public class Laboratorio_3 {

    /**
     * @param args the command line arguments
     */
    	public static void main(String[] arg){
		//Cree e inicialice dos objetos de tipo Punto y muestre la distancia entre ellos
		Punto punto1 = new Punto(1,1);
		Punto punto2 = new Punto(3,3);
		//System.out.println("Distancia entre punto1 y punto2: " + punto1.calcularDistanciaDesde(punto2));
                System.out.println("Distancia entre punto1 y punto2: " + punto1.calcularDistancia(punto2));
		
		//Cree un objeto de la clase Circulo y muestre su area, su per�metro y la
		//distancia entre el Circulo y uno de los Puntos creados en el punto anterior
		Punto centro = new Punto(5,5);
		Circulo circulo = new Circulo(centro, 6);
		System.out.println("Area del circulo: " + circulo.calcularArea());
		System.out.println("Perimetro del circulo: " + circulo.calcularPerimetro());
		//System.out.println("Distancia entre el circulo y el punto1: " + circulo.getCentro().calcularDistanciaDesde(punto1));
		
		//Cree un objeto de la clase Triangulo y muestre su area, su per�metro y la
		//distancia entre el Triangulo y un nuevo Puntos
		Triangulo triangulo = new Triangulo(0, 0, 5, 0, 6, 0);
		System.out.println("Area del Triangulo: " + triangulo.calcularArea());
		System.out.println("Perimetro del Traingulo: " + triangulo.calcularPerimetro());
		System.out.println("Distancia entre el Triangulo y un Punto: " + triangulo.calcularDistancia(punto2));
	}
    
}
