/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trigonomietria.basica;

/**
 *
 * @author anonimo
 */
public class Racionales {
    
		//atributos  
	 private long nume;
	 private long denom;
	  
	 	//constructor por defecto de la clase 
	 public  Racionales(){
		nume=0;denom=1;
	}
	 	//constructor que si ingresa un elemento entero
	 public Racionales(long elem){
	  nume= elem; denom=1;
	 }
	 
	 	//constructor cuando te ingresan numerador y denominador
	 public Racionales(long numerador, long denominador){
		 nume=numerador;
		 denom=denominador;
	 }
	 	

//////////////////////////////////////////////////////////////////////////// 
//Comienzo de la clase Racionales 
 
		//________________________________________________________________
		 //suma de Racionales 
		 public  Racionales sumaRac(Racionales o){
		  Racionales resultado = new Racionales (this.nume * o.denom + this.denom*o.nume,
					this.denom * o.denom);	 
		 return resultado;
		 }
		
		//_________________________________________________________________
		//resta de Racionales 
		 public Racionales restaRac(Racionales h){
			 Racionales resultado=new Racionales (this.nume*h.denom - this.denom*h.nume,
					 this.denom * h.denom);
			 return resultado;
		 }
		//________________________________________________________________
		// multilicacion de Racionales
		 public Racionales mulRac(Racionales g){
			 Racionales resultado = new Racionales (this.nume*g.nume,this.denom*g.denom);
			 return resultado;
		 }
		//_________________________________________________________________
		// division de Racionales 
		 public Racionales divi(Racionales b){
			Racionales resultado=new Racionales (this.nume*b.denom,this.denom*b.nume);
			return resultado;
		 }
		//__________________________________________________________________ 
		 // igualdada de Racionales
		 public boolean iguales(Racionales a){
			 return (this.nume == a.nume) && (this.denom == a.denom);
		 }
		 //__________________________________________________________________
		 //metodo ToString
	     public String toString(){	 
		 String res = new String ("["+nume+ "/" +denom+"]"); 
		 return res;
		 }
		////////////////////////////////////////////////////////////////////////////
		 //Fin de la clase Racionales
} 

