package laboratorio;

import trigonomietria.basica.Triangulo;
import trigonomietria.basica.Punto;
import trigonomietria.basica.Racionales;
import trigonomietria.basica.Circulo;
import trigonomietria.*;

public class Ejercicio1 {
	
	public static void main(String[] arg){
		//Cree e inicialice dos objetos de tipo Punto y muestre la distancia entre ellos
		Punto punto1 = new Punto(1,1);
		Punto punto2 = new Punto(3,3);
		System.out.println("Distancia entre punto1 y punto2: " + punto1.calcularDistanciaDesde(punto2));
		
		
		//Cree un objeto de la clase Circulo y muestre su area, su per�metro y la
		//distancia entre el Circulo y uno de los Puntos creados en el punto anterior
		Punto centro = new Punto(5,5);
		Circulo circulo = new Circulo(centro, 6);
		System.out.println("Area del circulo: " + circulo.calcularArea());
		System.out.println("Perimetro del circulo: " + circulo.calcularPerimetro());
		System.out.println("Distancia entre el circulo y el punto1: " + circulo.getCentro().calcularDistanciaDesde(punto1));
		
		//Cree un objeto de la clase Triangulo y muestre su area, su per�metro y la
		//distancia entre el Triangulo y un nuevo Puntos
		Triangulo triangulo = new Triangulo(0, 0, 5, 0, 6, 0);
		System.out.println("Area del Triangulo: " + triangulo.calcularArea());
		System.out.println("Perimetro del Traingulo: " + triangulo.calcularPerimetro());
		System.out.println("Distancia entre el Triangulo y un Punto: " + triangulo.calcularDistanciaDesde(punto2));
                
                
                //instancio en los tipos de las variables a utilizar con la clase racionales	  
                Racionales a = new Racionales ();
                Racionales b = new Racionales (3,4);

                System.out.println("2 + 3/4 = "+a.sumaRac(b)); //El toString me da el racional con el formato de salida
                System.out.println("1/2 - 3/4 = "+ a.restaRac(b)); //El toString me da el racional con el formato de salida
                System.out.println("1/2 * 3/4 = "+ a.mulRac(b)); //El toString me da el racional con el formato de salida
                System.out.println("1/2 / 3/4 = "+ a.divi(b)); //El toString me da el racional con el formato de salida
	}

}
