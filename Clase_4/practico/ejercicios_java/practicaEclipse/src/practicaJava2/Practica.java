package practicaJava2;

import java.util.Scanner;

public class Practica {

	public static void main(String[] args) {
		// 1. ​ Desarrollar un programa Java que muestre por pantalla el texto “¡Hola Mundo!” con
		//un editor de texto plano.    listo
		
		//2.​ Desarrollar un programa de las mismas características al anterior, pero utilizando el
		//IDE trabajado en clase.
		ejercicio2();
		
		
		//3. ​ Declarar dos variables numéricas (con el valor que desees), muestra por consola la
		//suma, resta, multiplicación, división y módulo (resto de la división).
		ejercicio3();
		
		
		//4.​ Declara 2 variables numéricas (con el valor que desees), he indica cual es mayor de los
		//dos. Si son iguales indicarlo también. Ves cambiando los valores para comprobar que
		//funciona.
		ejercicio4();
		
		
		//5. ​ Desarrollar un programa Java que muestre por pantalla los números primos del 1 al
		//1000 y todos los años bisiestos entre el año 2000 y el 3000.
		ejercicio5();
		
		
		
		//6.​ Desarrollar un programa Java que calcule el factorial de un número entero.
		ejercicio6(12);
		
		
		//7.​ Desarrollar un programa Java que sume los números del 1 al 100 (ambos inclusive).
		ejercicio7();
		
		
		//8. ​ Crea un array o arreglo unidimensional con un tamaño de 5, asignar los valores
		//numéricos manualmente (los que tu quieras) y mostrarlos por pantalla utilizando las
		//estructuras cíclicas vistas en clase.
		ejercicio8();
		
		
		//9. ​ Modifica el ejercicio anterior para que insertes los valores numéricos mediante un bucle
		//con consola y los muestre por pantalla.
		ejercicio9(5);
		
		
		//10. ​ Crea un array o arreglo unidimensional donde tu le indiques el tamaño por teclado y
		//crear una función que rellene el array o arreglo con los múltiplos de un número pedido por
		//teclado.
		//Por ejemplo, si defino un array de tamaño 5 y elijo un 3 en la función, el array contendrá 3,
		//6, 9, 12, 15. Mostrarlos por pantalla usando otra función distinta.
		ejercicio10();

		

	}


	private static String cualEsMayor(int param1, int param2) {
		if (param1 == param2)
			return "son iguales";
		if (param1 > param2)
			return "el primero es mayor";
		if (param1 < param2)
			return "el segundo es mayor";
		return null;
	}
	
	
	private static void ejercicio2() {
		System.out.println("2. --------------------");
		System.out.println("Hola Mundo");
		System.out.println("");
		System.out.println("");
	}

	
	private static void ejercicio3() {
		System.out.println("3. --------------------");
		int ej3_nro1 = 7;
		int ej3_nro2 = 5;
		System.out.println("Nro1: " + ej3_nro1);
		System.out.println("Nro2: " + ej3_nro2);
		System.out.println("Suma: " + (ej3_nro1 + ej3_nro2));
		System.out.println("Resta: " + (ej3_nro1 - ej3_nro2));
		System.out.println("Multiplicacion: " + (ej3_nro1 * ej3_nro2));
		System.out.println("Division: " + (ej3_nro1 / ej3_nro2));
		System.out.println("Modulo: " + (ej3_nro1 % ej3_nro2));
		System.out.println("");
		System.out.println("");		
	}
	
	
	private static void ejercicio4() {
		System.out.println("4. --------------------");
		int ej4_nro1 = 6;
		int ej4_nro2 = 3;
		System.out.println("Nro1: " + ej4_nro1);
		System.out.println("Nro2: " + ej4_nro2);
		System.out.println("El mayor es: " + cualEsMayor(ej4_nro1,ej4_nro2));
		ej4_nro1 = 6;
		ej4_nro2 = 9;
		System.out.println("Nro1: " + ej4_nro1);
		System.out.println("Nro2: " + ej4_nro2);
		System.out.println("El mayor es: " + cualEsMayor(ej4_nro1,ej4_nro2));
		ej4_nro1 = 6;
		ej4_nro2 = 6;
		System.out.println("Nro1: " + ej4_nro1);
		System.out.println("Nro2: " + ej4_nro2);
		System.out.println("El mayor es: " + cualEsMayor(ej4_nro1,ej4_nro2));
		System.out.println("");
		System.out.println("");	
	}
	
	
	private static void ejercicio5() {
		System.out.println("5. --------------------");
		int nro_primos = 1000;
		int bisiest_desde = 2000;
		int bisiest_hasta = 3000;
		
		primos(nro_primos);
		System.out.println("");
		
		bisiestos(bisiest_desde, bisiest_hasta);
	}

	private static void bisiestos(int desde, int hasta) {
		System.out.println("Bisiestos --------------------");
		for(int j = desde; j <= hasta; j++) {
			if(j%4 == 0)
				System.out.println("Nro: " + j);
		}		
		System.out.println("");
		System.out.println("");		
	}

	
	private static void primos(int param) {
		System.out.println("Primos --------------------");
		int v_cant_submultiplos;
		
		for(int i = 1; i <= param; i++) {
			v_cant_submultiplos = 0;
			
			//contar cant veces divisible
			for(int y = 1; y <= i; y++) {
				if(i%y == 0)
					v_cant_submultiplos++;
			}
			
			if (v_cant_submultiplos++ <= 2)
				System.out.println("Nro. " + i);
		}		
	}
	
	private static void ejercicio6(int param) {
		System.out.println("6. --------------------");
		int v_factorial = 1;
		System.out.println("El factorial de " + param);
		
		if(param == 0)
			System.out.println("Nro: 1");
		else {
			for(int i = 1; i < param; i++) {
				v_factorial = v_factorial * i;
			}
			System.out.println("Nro: " + v_factorial);
		}
		System.out.println("");
		System.out.println("");	
	}
	
	
	private static void ejercicio7() {
		System.out.println("7. --------------------");
		int v_sumaTotal = 0;
		
		for(int i = 0; i <= 100; i++) {
			v_sumaTotal = v_sumaTotal + i;
		}
		System.out.println("La suma de 1 a 100 es: " + v_sumaTotal);
		System.out.println("");
		System.out.println("");
		
	}
	
	

	private static void ejercicio8() {
		System.out.println("8. --------------------");
		
		int valores[] = new int[5];
		
		valores[0] = 7;
		valores[1] = 1;
		valores[2] = 8;
		valores[3] = 3;
		valores[4] = 9;
		
		System.out.println("Valores del vector:");
		
		for(int i = 0; i < valores.length; i++) {
			System.out.println("Valor vector[" + i + "]:" + valores[i]);
		}
		System.out.println("");
		System.out.println("");
		
	}
	
	
	private static void ejercicio9(int longitud) {
		System.out.println("9. --------------------");
		int valores[] = new int[longitud];
		Scanner input = new Scanner(System.in);
		
		for(int j = 0; j < longitud; j++) {
				System.out.println("Ingrese posicion " + j);
				//input.next();
				valores[j] = input.nextInt();

		}
		
		System.out.println("Valores del vector:");
		
		for(int i = 0; i < valores.length; i++) {
			System.out.println("Valor vector[" + i + "]:" + valores[i]);
		}
		
		input.close();
		
		System.out.println("");
		System.out.println("");
	}
	

	private static void ejercicio10() {
		System.out.println("10. --------------------");
		System.out.println("Ingrese la longitud: ");
		
		Scanner input = new Scanner(System.in);
		int longitud = input.nextInt();
		
		int valores[] = new int[longitud];		
		
		for(int j = 0; j < longitud; j++) {
				System.out.println("Ingrese posicion " + j);
				//input.next();
				valores[j] = input.nextInt();

		}
		
		System.out.println("Valores del vector:");
		
		for(int i = 0; i < valores.length; i++) {
			System.out.println("Valor vector[" + i + "]:" + valores[i]);
		}
		
		input.close();
		
		System.out.println("");
		System.out.println("");
		
	}
	
	
	
	//System.out.println("6. --------------------");
	
	//System.out.println("");
	//System.out.println("");
	
}
