/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicosclase4;

import java.util.Scanner;

/**
 *
 * @author anonimo
 */
public class PracticosClase4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        boolean ejecutarTodo = false;
        
        if (ejecutarTodo) {
            
            // 1. ​ Desarrollar un programa Java que muestre por pantalla el texto “¡Hola Mundo!” con
            //un editor de texto plano.    listo

            //2.​ Desarrollar un programa de las mismas características al anterior, pero utilizando el
            //IDE trabajado en clase.
            ejercicio2();

            //3. ​ Declarar dos variables numéricas (con el valor que desees), muestra por consola la
            //suma, resta, multiplicación, división y módulo (resto de la división).
            ejercicio3();

            //4.​ Declara 2 variables numéricas (con el valor que desees), he indica cual es mayor de los
            //dos. Si son iguales indicarlo también. Ves cambiando los valores para comprobar que
            //funciona.
            ejercicio4();

            //5. ​ Desarrollar un programa Java que muestre por pantalla los números primos del 1 al
            //1000 y todos los años bisiestos entre el año 2000 y el 3000.
            ejercicio5();

            //6.​ Desarrollar un programa Java que calcule el factorial de un número entero.
            ejercicio6(12);

            //7.​ Desarrollar un programa Java que sume los números del 1 al 100 (ambos inclusive).
            ejercicio7();

            //8. ​ Crea un array o arreglo unidimensional con un tamaño de 5, asignar los valores
            //numéricos manualmente (los que tu quieras) y mostrarlos por pantalla utilizando las
            //estructuras cíclicas vistas en clase.
            ejercicio8();

            //9. ​ Modifica el ejercicio anterior para que insertes los valores numéricos mediante un bucle
            //con consola y los muestre por pantalla.
            ejercicio9(5);

            //10. ​ Crea un array o arreglo unidimensional donde tu le indiques el tamaño por teclado y
            //crear una función que rellene el array o arreglo con los múltiplos de un número pedido por
            //teclado.
            //Por ejemplo, si defino un array de tamaño 5 y elijo un 3 en la función, el array contendrá 3,
            //6, 9, 12, 15. Mostrarlos por pantalla usando otra función distinta.
            ejercicio10();


            //11. ​ ​ Programa Java que lea 10 números enteros por teclado y los guarde en un array.
            //Calcula y muestra la media de los números que estén en las posiciones pares del array.
            ejercicio11();

            //12. ​ Programa Java que lea por teclado 10 números enteros y los guarde en un array. A
            //continuación calcula y muestra por separado la media de los valores positivos y la de los
            //valores negativos.
            ejercicio12();
            
            
            //13. ​ Lee un número por teclado e indica si es divisible entre 2 (resto = 0). Si no lo es,
            //también debemos indicarlo.
            ejercicio13();

            //14.​ Lee un número por teclado y muestra por consola, el carácter al que pertenece en la
            //tabla ASCII. Por ejemplo: si introduzco un 97, me muestre una a.
            ejercicio14();

            //15.​ Lee un número por teclado que pida el precio de un producto (puede tener decimales) y
            //calcule el precio con IVA. El IVA será una constante que será del 21%.
            ejercicio15();
            
            //16.​ Realiza una aplicación que nos pida un número de ventas a introducir, después nos
            //pedirá tantas ventas por teclado como número de ventas se hayan indicado. Al final
            //mostrará la suma de todas las ventas. Piensa que es lo que se repite y lo que no.
            ejercicio16();
            
            //17. ​ Realiza una aplicación que nos calcule una ecuación de segundo grado. Debes pedir
            //las variables a, b y c por teclado y comprobar antes que el discriminante (operación en la
            //raíz cuadrada). Para la raíz cuadrada usa el método sqlrt de Math. Te recomiendo que uses
            //mensajes de traza.
            ejercicio17();
            
            //18.​ Lee un número por teclado y comprueba que este número es mayor o igual que cero,
            //si no lo es lo volverá a pedir (do while), después muestra ese número por consola.
            ejercicio18();
            
            //19.​ Escribe una aplicación con un String que contenga una contraseña cualquiera.
            //Después se te pedirá que introduzcas la contraseña, con 3 intentos. Cuando aciertes ya
            //no pedirá mas la contraseña y mostrara un mensaje diciendo “Enhorabuena”. Piensa bien
            //en la condición de salida (3 intentos y si acierta sale, aunque le queden intentos).
            ejercicio19();
        }
        

        
        //20.​ Pide por teclado dos número y genera 10 números aleatorios entre esos números.
        //Usa el método Math.random para generar un número entero aleatorio (recuerda el
        //casting de double a int).
        ejercicio20();

    }

    private static String cualEsMayor(int param1, int param2) {
        if (param1 == param2) {
            return "son iguales";
        }
        if (param1 > param2) {
            return "el primero es mayor";
        }
        if (param1 < param2) {
            return "el segundo es mayor";
        }
        return null;
    }

    private static void ejercicio2() {
        //2.​ Desarrollar un programa de las mismas características al anterior, pero utilizando el
        //IDE trabajado en clase.
        System.out.println("2. --------------------");
        System.out.println("Hola Mundo");
        System.out.println("");
        System.out.println("");
    }

    private static void ejercicio3() {
        //3. ​ Declarar dos variables numéricas (con el valor que desees), muestra por consola la
        //suma, resta, multiplicación, división y módulo (resto de la división).
        System.out.println("3. --------------------");
        int ej3_nro1 = 7;
        int ej3_nro2 = 5;
        System.out.println("Nro1: " + ej3_nro1);
        System.out.println("Nro2: " + ej3_nro2);
        System.out.println("Suma: " + (ej3_nro1 + ej3_nro2));
        System.out.println("Resta: " + (ej3_nro1 - ej3_nro2));
        System.out.println("Multiplicacion: " + (ej3_nro1 * ej3_nro2));
        System.out.println("Division: " + (ej3_nro1 / ej3_nro2));
        System.out.println("Modulo: " + (ej3_nro1 % ej3_nro2));
        System.out.println("");
        System.out.println("");
    }

    private static void ejercicio4() {
        //4.​ Declara 2 variables numéricas (con el valor que desees), he indica cual es mayor de los
        //dos. Si son iguales indicarlo también. Ves cambiando los valores para comprobar que
        //funciona.
        System.out.println("4. --------------------");
        int ej4_nro1 = 6;
        int ej4_nro2 = 3;
        System.out.println("Nro1: " + ej4_nro1);
        System.out.println("Nro2: " + ej4_nro2);
        System.out.println("El mayor es: " + cualEsMayor(ej4_nro1, ej4_nro2));
        ej4_nro1 = 6;
        ej4_nro2 = 9;
        System.out.println("Nro1: " + ej4_nro1);
        System.out.println("Nro2: " + ej4_nro2);
        System.out.println("El mayor es: " + cualEsMayor(ej4_nro1, ej4_nro2));
        ej4_nro1 = 6;
        ej4_nro2 = 6;
        System.out.println("Nro1: " + ej4_nro1);
        System.out.println("Nro2: " + ej4_nro2);
        System.out.println("El mayor es: " + cualEsMayor(ej4_nro1, ej4_nro2));
        System.out.println("");
        System.out.println("");
    }

    private static void ejercicio5() {
        //5. ​ Desarrollar un programa Java que muestre por pantalla los números primos del 1 al
        //1000 y todos los años bisiestos entre el año 2000 y el 3000.
        System.out.println("5. --------------------");
        int nro_primos = 1000;
        int bisiest_desde = 2000;
        int bisiest_hasta = 3000;

        primos(nro_primos);
        System.out.println("");

        bisiestos(bisiest_desde, bisiest_hasta);
    }

    private static void bisiestos(int desde, int hasta) {
        System.out.println("Bisiestos --------------------");
        for (int j = desde; j <= hasta; j++) {
            if (j % 4 == 0) {
                System.out.println("Nro: " + j);
            }
        }
        System.out.println("");
        System.out.println("");
    }

    private static void primos(int param) {
        System.out.println("Primos --------------------");
        int v_cant_submultiplos;

        for (int i = 1; i <= param; i++) {
            v_cant_submultiplos = 0;

            //contar cant veces divisible
            for (int y = 1; y <= i; y++) {
                if (i % y == 0) {
                    v_cant_submultiplos++;
                }
            }

            if (v_cant_submultiplos++ <= 2) {
                System.out.println("Nro. " + i);
            }
        }
    }

    private static void ejercicio6(int param) {
        System.out.println("6. --------------------");
        int v_factorial = 1;
        System.out.println("El factorial de " + param);

        if (param == 0) {
            System.out.println("Nro: 1");
        } else {
            for (int i = 1; i < param; i++) {
                v_factorial = v_factorial * i;
            }
            System.out.println("Nro: " + v_factorial);
        }
        System.out.println("");
        System.out.println("");
    }

    private static void ejercicio7() {
        //7.​ Desarrollar un programa Java que sume los números del 1 al 100 (ambos inclusive).
        System.out.println("7. --------------------");
        int v_sumaTotal = 0;

        for (int i = 0; i <= 100; i++) {
            v_sumaTotal = v_sumaTotal + i;
        }
        System.out.println("La suma de 1 a 100 es: " + v_sumaTotal);
        System.out.println("");
        System.out.println("");

    }

    private static void ejercicio8() {
        //8. ​ Crea un array o arreglo unidimensional con un tamaño de 5, asignar los valores
        //numéricos manualmente (los que tu quieras) y mostrarlos por pantalla utilizando las
        //estructuras cíclicas vistas en clase.
        System.out.println("8. --------------------");

        int valores[] = new int[5];

        valores[0] = 7;
        valores[1] = 1;
        valores[2] = 8;
        valores[3] = 3;
        valores[4] = 9;

        System.out.println("Valores del vector:");

        for (int i = 0; i < valores.length; i++) {
            System.out.println("Valor vector[" + i + "]:" + valores[i]);
        }
        System.out.println("");
        System.out.println("");
    }

    private static void ejercicio9(int longitud) {
        //9. ​ Modifica el ejercicio anterior para que insertes los valores numéricos mediante un bucle
        //con consola y los muestre por pantalla.
        System.out.println("9. --------------------");
        int valores[] = new int[longitud];
        Scanner input = new Scanner(System.in);

        for (int j = 0; j < longitud; j++) {
            System.out.println("Ingrese posicion " + j);
            //input.next();
            valores[j] = input.nextInt();
        }

        System.out.println("Valores del vector:");

        for (int i = 0; i < valores.length; i++) {
            System.out.println("Valor vector[" + i + "]:" + valores[i]);
        }

        //input.close();
        //input = null;

        System.out.println("");
        System.out.println("");
    }

    private static void ejercicio10() {
        //10. ​ Crea un array o arreglo unidimensional donde tu le indiques el tamaño por teclado y
        //crear una función que rellene el array o arreglo con los múltiplos de un número pedido por
        //teclado.
        //Por ejemplo, si defino un array de tamaño 5 y elijo un 3 en la función, el array contendrá 3,
        //6, 9, 12, 15. Mostrarlos por pantalla usando otra función distinta.
        System.out.println("10. --------------------");
        
        Scanner input2 = new Scanner(System.in);
        int longit;
        int valores[];
        
        System.out.println("Ingrese la longitud: ");
        longit = input2.nextInt();
        valores = new int[longit];
        
        completarArray(valores);
        
        imprimirArray(valores);   

        //input2.close();
        //input2 = null;

        System.out.println("");
        System.out.println("");
    }

    private static void completarArray(int[] valores) {
        Scanner input = new Scanner(System.in);
        int multiplo;
        
        System.out.println("Ingrese el multiplo: ");
        multiplo = input.nextInt();
        
        for(int i = 0; i < valores.length; i++){
            valores[i] = (i+1) * multiplo;
        }
        
        //input.close();        
    }

    private static void imprimirArray(int[] valores) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        System.out.println("Valores del vector:");

        for (int i = 0; i < valores.length; i++) {
            System.out.println("Valor vector[" + i + "]:" + valores[i]);
        }
    }
    
    
    private static void ejercicio11() {
        //11. ​ ​ Programa Java que lea 10 números enteros por teclado y los guarde en un array.
        //Calcula y muestra la media de los números que estén en las posiciones pares del array.
        System.out.println("11. --------------------");
        
        Scanner input = new Scanner(System.in);
        int valores[] = new int[10]; 
        int mediaPares;
        
        for (int j = 0; j < valores.length; j++) {
            System.out.println("Ingrese posicion " + j);
            valores[j] = input.nextInt();
        }
        
        mediaPares = calcularPares(valores);
        System.out.println("La media es: " + mediaPares);
    }

    private static int calcularPares(int[] valores) {
        int media = 0;
        
        for(int i = 0; i < valores.length; i++){
            if((i+1)%2 == 0){
                media += valores[i];
            }            
        }
        
        media = media / (int)(valores.length/2);
        
        return media;
    }

    private static void ejercicio12() {
        //12. ​ Programa Java que lea por teclado 10 números enteros y los guarde en un array. A
        //continuación calcula y muestra por separado la media de los valores positivos y la de los
        //valores negativos.
        System.out.println("12. --------------------");
        
        Scanner input = new Scanner(System.in);
        int valores[] = new int[10]; 
        int mediaPares = 0;
        int mediaImpares = 0;
        int cantPares = 0;
        int cantImpares = 0;
        
        for (int j = 0; j < valores.length; j++) {
            System.out.println("Ingrese posicion " + j);
            valores[j] = input.nextInt();
        }
        
        for(int i = 0; i < valores.length; i++){
            if (valores[i]>=0){
                mediaPares = mediaPares + valores[i];
                cantPares++;
            }
            else{
                mediaImpares = mediaImpares + valores[i];
                cantImpares++;
            }
        }
        
        if(cantPares > 0)
            System.out.println("Media Pares: " + (mediaPares / cantPares));
        else
            System.out.println("No hay valores Pares");
        
        if(cantImpares > 0)
            System.out.println("Media Impares: " + (mediaImpares / cantImpares));
        else
            System.out.println("No hay valores Impares");
        
        System.out.println("");
        System.out.println("");
    }
    
    
    private static void ejercicio13() {
        //13. ​ Lee un número por teclado e indica si es divisible entre 2 (resto = 0). Si no lo es,
        //también debemos indicarlo.
        System.out.println("13. --------------------");
        
        Scanner input = new Scanner(System.in);
        int valores = 0; 
        
        System.out.println("Ingrese un nro: ");
        valores = input.nextInt();
        
        if (valores%2 == 0)
            System.out.println("Es divisible por 2");
        else
            System.out.println("NO es divisible por 2");
        
        System.out.println("");
        System.out.println("");
    }
    
    
    private static void ejercicio14() {
        //14.​ Lee un número por teclado y muestra por consola, el carácter al que pertenece en la
        //tabla ASCII. Por ejemplo: si introduzco un 97, me muestre una a.
        System.out.println("14. --------------------");
        
        Scanner input = new Scanner(System.in);
        int valor = 0; 
        char convertir;
        
        System.out.println("Ingrese un nro para convertir en ASCII: ");
        valor = input.nextInt();
        
        convertir = (char) valor;
        System.out.println("El caracter en ASCII es: " + convertir);
        
        System.out.println("");
        System.out.println("");
    }    

    private static void ejercicio15() {
        //15.​ Lee un número por teclado que pida el precio de un producto (puede tener decimales) y
        //calcule el precio con IVA. El IVA será una constante que será del 21%.
        System.out.println("15. --------------------");
        
        Scanner input = new Scanner(System.in);
        double precio = 0; 
        final double iva = 1.21;
        double total;
        
        System.out.println("Ingrese el precio SIN IVA (con coma, NO punto): ");
        precio = input.nextDouble();
        
        total = precio * iva;
        System.out.println("Precio CON IVA: " + total);
        
        System.out.println("");
        System.out.println("");
    }
    
    
    private static void ejercicio16() {
        //16.​ Realiza una aplicación que nos pida un número de ventas a introducir, después nos
        //pedirá tantas ventas por teclado como número de ventas se hayan indicado. Al final
        //mostrará la suma de todas las ventas. Piensa que es lo que se repite y lo que no.
        System.out.println("16. --------------------");
        
        Scanner input = new Scanner(System.in);
        int cantVentas = 0; 
        double monto = 0;
        double total = 0;
        
        System.out.println("Ingrese la cantidad de ventas: ");
        cantVentas = input.nextInt();
        
        for(int i = 0; i < cantVentas; i++){
            System.out.println("Ingrese el monto de la venta " + (i+1) + " (usar coma): ");
            monto = input.nextDouble();
            total += monto;
        }
        
        System.out.println("El total vendido es: " + total);
        
        
        
        System.out.println("");
        System.out.println("");
    }    
    
 
    private static void ejercicio17() {
        //17. ​ Realiza una aplicación que nos calcule una ecuación de segundo grado. Debes pedir
        //las variables a, b y c por teclado y comprobar antes que el discriminante (operación en la
        //raíz cuadrada). Para la raíz cuadrada usa el método sqlrt de Math. Te recomiendo que uses
        //mensajes de traza.
        System.out.println("17. --------------------");
        
        Scanner input = new Scanner(System.in);
        double variableA = 0.0; 
        double variableB = 0.0; 
        double variableC = 0.0; 
        double resultado1 = 0.0;
        double resultado2 = 0.0;
        
        do {
            System.out.println("Ingrese la variable A (distinto de cero): ");
            variableA = input.nextDouble();
        }while(variableA == 0);
        
        System.out.println("Ingrese la variable B: ");
        variableB = input.nextDouble();
        System.out.println("Ingrese la variable C: ");
        variableC = input.nextDouble();
        
        System.out.println("valor de A: " + variableA);
        System.out.println("valor de B: " + variableB);
        System.out.println("valor de C: " + variableC);
        
        resultado1 = ((-variableB) + Math.sqrt(Math.pow(variableB, 2) - (4 * variableA * variableC))) / (2 * variableA);
        resultado2 = ((-variableB) - Math.sqrt(Math.pow(variableB, 2) - (4 * variableA * variableC))) /  (2 * variableA);
        
        System.out.println("El primer resultado es: " + resultado1);   //VER devuelve NaN
        System.out.println("El segundo resultado es: " + resultado2);  //VER devuelve NaN
        
        System.out.println("");
        System.out.println("");
    }

    private static void ejercicio18() {
        //18.​ Lee un número por teclado y comprueba que este número es mayor o igual que cero,
        //si no lo es lo volverá a pedir (do while), después muestra ese número por consola.
        System.out.println("18. --------------------");
        
        Scanner input = new Scanner(System.in);
        int variableA = 0; 
        
        do {
            System.out.println("Ingrese un valor (mayor o igual a cero): ");
            variableA = input.nextInt();
        }while(variableA < 0);
        
        System.out.println("El valor ingresado es: " + variableA);           
        
        
        System.out.println("");
        System.out.println("");
        
    }

    private static void ejercicio19() {
        //19.​ Escribe una aplicación con un String que contenga una contraseña cualquiera.
        //Después se te pedirá que introduzcas la contraseña, con 3 intentos. Cuando aciertes ya
        //no pedirá mas la contraseña y mostrara un mensaje diciendo “Enhorabuena”. Piensa bien
        //en la condición de salida (3 intentos y si acierta sale, aunque le queden intentos).
        System.out.println("19. --------------------");
        
        Scanner input = new Scanner(System.in);
        String contraseña = "sebas";
        String ingreso;
        int intentos = 0;
        boolean correcto = false;
        
        do {
            System.out.println("Ingrese la contraseña: ");
            ingreso = input.nextLine();
            
            if (ingreso.compareTo(contraseña)==0)
                correcto = true;
            else
                intentos++;
            
        }while((!correcto) && intentos < 3);
        
        if (correcto)
            System.out.println("La contraseña es correcta");           
        else
            System.out.println("La contraseña es incorrecta");           
        
        System.out.println("");
        System.out.println("");
    }

    private static void ejercicio20() {
        //20.​ Pide por teclado dos número y genera 10 números aleatorios entre esos números.
        //Usa el método Math.random para generar un número entero aleatorio (recuerda el
        //casting de double a int).
        System.out.println("20. --------------------");
        
        Scanner input = new Scanner(System.in);
        int primero = 0;
        int segundo = 0;
        int aleatorio = 0;
        
        System.out.println("Ingrese el primer valor: ");
        primero = input.nextInt();
        
        System.out.println("Ingrese el segundo valor: ");
        segundo = input.nextInt();
        
        for(int i = 0; i < 10; i++){
            //aleatorio = (int) (Math.random() * segundo) + primero;
            aleatorio = (int) Math.floor(Math.random() * (segundo-primero+1) + primero);
            System.out.println("random " + (i+1) + ": " + aleatorio);
        }
        
        
        System.out.println("");
        System.out.println("");
    }


    
}
