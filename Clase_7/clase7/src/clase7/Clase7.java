/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase7;

/**
 *
 * @author anonimo
 */
public class Clase7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Numero n1 = new Numero();
        Numero n2 = new Numero();
        Numero n3 = new Numero();
        
        System.out.println("Estatico " + Numero.getValor_estatico());
        Numero.setValor_estatico(22);
        System.out.println("Estatico " + Numero.getValor_estatico());
        
        n1.set_otro_valor(2);
        n2.set_otro_valor(7);
        n3.set_otro_valor(10);
        
        System.out.println("n1: " + n1.toString());
        System.out.println("n2: " + n2.toString());
        System.out.println("n3: " + n3.toString());
        
        System.out.println("Estatico " + Numero.getValor_estatico());
        
    }
    
}
